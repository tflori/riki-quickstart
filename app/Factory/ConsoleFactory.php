<?php

namespace App\Factory;

use Hugga\Console;

class ConsoleFactory extends AbstractFactory
{
    protected $shared = true;

    /**
     * @return Console
     * @codeCoverageIgnore ConsoleFactory gets mocked in tests
     */
    protected function build()
    {
        return new Console($this->container->logger);
    }
}
