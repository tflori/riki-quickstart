<?php

namespace App\Factory;

use App\Http\HttpKernel;

/** @codeCoverageIgnore trivial */
class RequestFactory extends AbstractFactory
{
    protected function build()
    {
        return HttpKernel::currentRequest();
    }
}
