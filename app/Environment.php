<?php

namespace App;

/**
 * Class Environment
 *
 * This class defines specifics for the current environment. The application detects based on
 * environment variable on which environment the app is running and uses the specific App\Environment\* class
 * if available.
 *
 * This is the right place to configure environment specific behaviour of your application. For example:
 *   - can error messages be shown in http responses?
 *   - where are log files stored?
 *   - where and if the configuration can be cached?
 *
 * @package App
 * @codeCoverageIgnore trivial code
 */
class Environment extends \Riki\Environment
{
    public function canShowErrors(): bool
    {
        return false;
    }

    public function storagePath(string ...$path): string
    {
        return $this->path('storage', ...$path);
    }

    public function logPath(string ...$path): string
    {
        return $this->storagePath('logs', ...$path);
    }

    public function cachePath(string ...$path): string
    {
        return $this->storagePath('cache', ...$path);
    }

    public function viewPath(string ...$path): string
    {
        return $this->path('resources', 'views', ...$path);
    }

    public function getConfigCachePath(): string
    {
        return $this->cachePath('config.spo');
    }

    protected function path(string ...$path)
    {
        array_unshift($path, $this->getBasePath());
        return implode(DIRECTORY_SEPARATOR, $path);
    }
}
