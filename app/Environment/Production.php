<?php

namespace App\Environment;

use App\Environment;

/**
 * Class Production
 *
 * This environment will be loaded when the APP_ENV variable is set to production. Typically, the production
 * environment has the same setup then the App\Environment. It could change however paths that are different
 * or if a logging service should be used.
 *
 * @codeCoverageIgnore Environment will not be loaded in tests
 */
class Production extends Environment
{
}
