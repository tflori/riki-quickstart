<?php

namespace App\Environment;

use App\Environment;

/**
 * Class Development
 *
 * Typically, this environment gets loaded when you omit APP_ENV environment variable. It defines
 * the differences on your development environment. You only need to overwrite differences to the default environment.
 *
 * @codeCoverageIgnore Environment will not be loaded in tests
 */
class Development extends Environment
{
    public function canCacheConfig(): bool
    {
        return false;
    }

    public function canShowErrors(): bool
    {
        return true;
    }
}
