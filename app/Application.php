<?php

namespace App;

use Hugga\Console;
use Monolog\Logger;
use Whoops;
use Whoops\Handler\PlainTextHandler;

/**
 * Class Application
 *
 * @package App
 *
 * @method static Application app()
 * @method static Environment environment()
 * @method static Config config()
 * @method static Logger logger()
 * @method static Console console()
 * @method static Whoops\Run whoops()
 * @property-read Application $app
 * @property-read Environment $environment
 * @property-read Config $config
 * @property-read Logger $logger
 * @property-read Console $console
 * @property-read Whoops\Run $whoops
 */
class Application extends \Riki\Application
{
    public function __construct(string $basePath)
    {
        parent::__construct($basePath);
        $this->initWhoops();
    }

    protected function initDependencies()
    {
        parent::initDependencies();

        // Register a namespace for factories
        $this->registerNamespace('App\Factory', 'Factory');

        // Register Whoops\Run under whoops
        $this->instance('whoops', new Whoops\Run());
    }

    public function initWhoops(): void
    {
        $whoops = $this->whoops;
        $plainTextHandler = new PlainTextHandler($this->logger);
        $plainTextHandler->loggerOnly(true);
        $whoops->appendHandler($plainTextHandler);
        $whoops->register();
    }

    public function run(\Riki\Kernel $kernel, ...$args)
    {
        if ($kernel instanceof Kernel) {
            $errorHandlers = $kernel->getErrorHandlers();
            foreach ($errorHandlers as $handler) {
                $this->whoops->appendHandler($handler);
            }
        }

        $result = parent::run($kernel, ...$args);

        if (!empty($errorHandlers)) {
            for ($i = 0; $i < count($errorHandlers); $i++) {
                $this->whoops->removeLastHandler();
            }
        }

        return $result;
    }
}
