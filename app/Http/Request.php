<?php

namespace App\Http;

use App\Application;
use App\Service\IpHelper;
use Tal\ServerRequest;

class Request extends ServerRequest
{
    const REAL_IP_HEADER = ['X-Real-Ip', 'X-Forwarded-For'];

    /** @var array */
    protected $input;

    /** @codeCoverageIgnore */
    public function __clone()
    {
        $this->input = null;
    }

    /** @codeCoverageIgnore */
    public function __get($name)
    {
        $this->get($name);
    }

    /**
     * Get the IP of the client
     *
     * If we run behind a reversed proxy make sure to set the TRUSTED_PROXIES variable accordingly.
     *
     * Returns the x-real-ip and x-forwarded-for header when the proxy is trusted.
     *
     * @return string
     */
    public function getIp(): string
    {
        $remoteAddr = $this->serverParams['REMOTE_ADDR'] ?? '127.0.0.1';

        if (!$this->isTrustedForward()) {
            return $remoteAddr;
        }

        foreach (static::REAL_IP_HEADER as $header) {
            if ($this->hasHeader($header)) {
                $forwardedFor = array_reverse(array_map('trim', explode(',', $this->getHeader($header)[0])));
                return $forwardedFor[0];
            }
        }

        return $remoteAddr;
    }

    /**
     * Get the currently used protocol
     *
     * If we run behind a reversed proxy make sure to set the TRUSTED_PROXIES variable accordingly.
     *
     * Returns the x-forwarded-proto header when the proxy is trusted.
     *
     * @return string
     */
    public function getProtocol(): string
    {
        // any non-empty value means it is using https except the value 'off'
        $currentProtocol = ($this->serverParams['HTTPS'] ?? 'off') !== 'off' ? 'https' : 'http';

        if (!$this->isTrustedForward()) {
            return $currentProtocol;
        }

        if ($this->hasHeader('X-Forwarded-Proto')) {
            return $this->getHeader('X-Forwarded-Proto')[0];
        }

        return $currentProtocol;
    }

    /**
     * Was the request an ssl secured request
     *
     * You might want to return an error response when the request was not secured via ssl.
     *
     * @return bool
     */
    public function isSslSecured(): bool
    {
        return $this->getProtocol() === 'https';
    }

    /**
     * Check if the proxy is a trusted proxy.
     *
     * Returns whether the direct client ip ($_SERVER['REMOTE_ADDR']) matches against one of the defined proxies
     * in $config->trustedProxies.
     *
     * $config->trustedProxies is an array of ip addresses, address ranges or a partial reg ex pattern.
     *
     * Examples:
     * ```php
     * $config->trustedProxies = [
     *     '192.168.0.1', // ipv4 of our reverse proxy (only one proxy)
     *     '42.42.42.64/28', // ipv4 subnet (the proxy comes from this subnet)
     *     'fe80::/64', //  local ipv6 range
     * ];
     *```
     *
     * @return bool
     */
    public function isTrustedForward(): bool
    {
        $config = Application::config();
        if (empty($config->trustedProxies)) {
            return false;
        }

        foreach ($config->trustedProxies as $ipRange) {
            if (IpHelper::isInRange($this->serverParams['REMOTE_ADDR'] ?? '127.0.0.1', $ipRange)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if $mimeType is accepted by the request
     *
     * @param string $mimeType
     * @return bool
     */
    public function accepts(string $mimeType): bool
    {
        list($type, $subType) = explode('/', $mimeType);
        $re = '/(^|, ?)' . preg_quote($type, '/') . '\/(\*|' . preg_quote($subType, '/') . ')(;|,|$)/i';
        return $this->hasHeader('Accept') && preg_match($re, $this->getHeader('Accept')[0]);
    }

    public function get(string $key = null, $default = null)
    {
        if (!$this->input) {
            $body = $this->getJson() ?? $this->getPost();
            $this->input = is_array($body) ? array_merge($this->getQuery(), $body) : $this->getQuery();
        }

        if (!$key) {
            return $this->input;
        }

        return $this->input[$key] ?? $default;
    }

    /**
     * Get all params or the parameter $key from query
     *
     * @param string $key
     * @param mixed $default
     * @return array|mixed
     */
    public function getQuery(string $key = null, $default = null)
    {
        if (!$key) {
            return $this->getQueryParams();
        }

        return $this->getQueryParams()[$key] ?? $default;
    }

    /**
     * Get all params or the parameter $key from post body
     *
     * @param string $key
     * @param mixed $default
     * @return array|mixed
     */
    public function getPost(string $key = null, $default = null)
    {
        if (!$key) {
            return $this->getParsedBody();
        }

        return $this->getParsedBody()[$key] ?? $default;
    }

    /**
     * Get the parsed json from request body
     *
     * Warning: $assoc defaults to true in this method.
     *
     * @param bool $assoc
     * @param int  $depth
     * @param int  $options
     * @see json_decode()
     * @return mixed
     */
    public function getJson(bool $assoc = true, int $depth = 512, int $options = 0)
    {
        if ($this->hasHeader('Content-Type') &&
            strtolower($this->getHeader('Content-Type')[0]) !== 'application/json'
        ) {
            return null;
        }

        $data = json_decode((string)$this->getBody(), $assoc, $depth, $options);

        if ($data === null && trim((string)$this->getBody()) !== 'null') {
            Application::logger()->notice(sprintf('Invalid json provided in body: \'%s\'', $this->getBody()));
        }

        return $data;
    }
}
