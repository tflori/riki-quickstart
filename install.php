<?php

$composerConfig = json_decode(file_get_contents(__DIR__ . '/composer.json'), true);
unset(
    $composerConfig['scripts']['post-create-project-cmd'],
    $composerConfig['archive'],
);
$composerConfig['name'] = get_current_user() . '/' . basename(__DIR__);
$composerConfig['description'] = '';
$composerConfig['license'] = 'proprietary';
file_put_contents(__DIR__ . '/composer.json', json_encode($composerConfig, JSON_PRETTY_PRINT));

$gitignore = file(__DIR__ . '/.gitignore');
if (($key = array_search("/composer.lock\n", $gitignore)) !== false) {
    unset($gitignore[$key]);
}
file_put_contents(__DIR__ . '/.gitignore', implode('', $gitignore));

?>
########################################################################################
# Thank you for choosing riki-framework!                                               #
#                                                                                      #
# You may want to update the composer configuration in composer.json before you start. #
#                                                                                      #
# To run a development server just use `composer start`.                               #
########################################################################################
<?php

!file_exists('.gitlab-ci.yml') || unlink('.gitlab-ci.yml');
unlink(__FILE__);
