<?php

namespace Test\Unit\Http;

use App\Http\Request;
use GuzzleHttp\Psr7\Utils;
use InvalidArgumentException;
use Mockery as m;
use Test\TestCase;
use function GuzzleHttp\Psr7\stream_for;

class RequestTest extends TestCase
{
    /** @test */
    public function getQueryReturnsTheCompleteQuery()
    {
        $request = (new Request('GET', '/any/path'))
            ->withQueryParams(['foo' => 42, 'bar' => 23]);

        $query = $request->getQuery();

        self::assertSame(['foo' => 42, 'bar' => 23], $query);
    }

    /** @test */
    public function getQueryReturnsSpecificParameter()
    {
        $request = (new Request('GET', '/any/path'))
            ->withQueryParams(['foo' => 42, 'bar' => 23]);

        $query = $request->getQuery('foo');

        self::assertSame(42, $query);
    }

    /** @test */
    public function getQueryReturnsTheDefaultValue()
    {
        $request = new Request('GET', '/any/path');

        $query = $request->getQuery('foo', 42);

        self::assertSame(42, $query);
    }

    /** @test */
    public function getPostReturnsTheCompletePost()
    {
        $request = (new Request('POST', '/any/path'))
            ->withParsedBody(['foo' => 42, 'bar' => 23]);

        $post = $request->getPost();

        self::assertSame(['foo' => 42, 'bar' => 23], $post);
    }

    /** @test */
    public function getPostReturnsSpecificParameter()
    {
        $request = (new Request('POST', '/any/path'))
            ->withParsedBody(['foo' => 42, 'bar' => 23]);

        $post = $request->getPost('foo');

        self::assertSame(42, $post);
    }

    /** @test */
    public function getPostReturnsTheDefaultValue()
    {
        $request = new Request('POST', '/any/path');

        $post = $request->getPost('foo', 42);

        self::assertSame(42, $post);
    }

    /** @test */
    public function getJsonReturnsTheRequestBodyJsonDecoded()
    {
        $data = [
            'foo' => 42,
            'bar' => 23,
            'baz' => null,
        ];
        $request = (new Request('POST', '/any/path'))
            ->withBody(Utils::streamFor(json_encode($data)));

        self::assertSame($data, $request->getJson());
    }

    /** @test */
    public function getJsonRetrunsNullWhenTheContentTypeHeaderIsNotJson()
    {
        $request = (new Request('POST', '/any/path', [
            'Content-Type' => 'text/plain',
        ]))->withBody(Utils::streamFor(json_encode(['a' => 'b'])));

        self::assertNull($request->getJson());
    }

    /** @test */
    public function getJsonLogsNoticeWhenBodyIsInvalid()
    {
        $request = (new Request('POST', '/any/path'))
            ->withBody(Utils::streamFor("{foo:'bar'}")); // this is not json but javascript

        $this->mocks['logger']->shouldReceive('notice')->once();

        $request->getJson();
    }

    /** @test */
    public function getJsonThrowsNotWhenTheJsonIsNull()
    {
        $request = (new Request('POST', '/any/path'))
            ->withBody(Utils::streamFor(json_encode(null)));

        self::assertNull($request->getJson());
    }

    /** @test */
    public function getIpReturnsTheRemoteAddr()
    {
        $request = (new Request('GET', '/any/path', [], null, '1.1', [
            'REMOTE_ADDR' => '172.19.0.9',
        ]));

        $ip = $request->getIp();

        self::assertSame('172.19.0.9', $ip);
    }

    /** @dataProvider provideIpHeaders
     * @test */
    public function getIpReturnsTheRealIpWhenProxyIsTrusted(array $header, string $remoteAddr, string $expected)
    {
        $config = $this->app->config;
        $config->trustedProxies = [$remoteAddr];
        $request = (new Request('GET', '/any/path', $header, null, '1.1', [
            'REMOTE_ADDR' => $remoteAddr,
        ]));

        $ip = $request->getIp();

        self::assertSame($expected, $ip);
    }

    public function provideIpHeaders()
    {
        return [
            'X-Real-Ip' => [['X-Real-Ip' => '8.8.8.8'], '10.0.0.1', '8.8.8.8'],
            'X-Forwarded-For' => [['X-Forwarded-For' => '8.8.8.8'], '10.0.0.1', '8.8.8.8'],
            'precedence' => [[ // prefers x-real-ip
                'X-Forwarded-For' => '23.0.4.2',
                'X-Real-Ip' => '8.8.8.8',
            ], '10.0.0.1', '8.8.8.8'],
            'last-entry' => [[ // uses the last entry
                'X-Forwarded-For' => '23.0.4.2, 8.8.8.8',
            ], '10.0.0.1', '8.8.8.8'],
        ];
    }

    /** @test */
    public function getIpReturnsTheRemoteAddrIfNoRealIpHeaderGiven()
    {
        $config = $this->app->config;
        $config->trustedProxies = ['10.0.0.1'];
        $request = (new Request('GET', '/any/path', [], null, '1.1', [
            'REMOTE_ADDR' => '10.0.0.1',
        ]));

        $ip = $request->getIp();

        self::assertSame('10.0.0.1', $ip);
    }

    /** @test */
    public function getIpReturnsTheRemoteAddrWhenProxyIsUntrusted()
    {
        /** @var Request|m\MockInterface $request */
        $request = m::mock(Request::class)->makePartial();
        $request->__construct('GET', '/any/path', [
            'X-Forwarded-For' => '23.0.4.2'
        ], null, '1.1', [
            'REMOTE_ADDR' => '8.8.8.8',
        ]);
        $request->shouldReceive('isTrustedForward')->once()->andReturnFalse();

        $ip = $request->getIp();

        self::assertSame('8.8.8.8', $ip);
    }

    /** @test */
    public function getReturnsTheValueFromQuery()
    {
        $request = (new Request('GET', '/any/path'))
            ->withQueryParams(['foo' => 'bar']);

        self::assertSame('bar', $request->get('foo'));
    }

    /** @test */
    public function getReturnsTheValueFromPost()
    {
        $request = (new Request('GET', '/any/path'))
            ->withQueryParams(['foo' => 'bar'])
            ->withParsedBody(['foo' => 'baz']);

        self::assertSame('baz', $request->get('foo'));
    }

    /** @test */
    public function getReturnsTheValueFromJson()
    {
        $request = (new Request('GET', '/any/path'))
            ->withQueryParams(['foo' => 'bar'])
            ->withBody(Utils::streamFor(json_encode(['foo' => 'baz'])));

        self::assertSame('baz', $request->get('foo'));
    }

    /** @test */
    public function getReturnsTheMergedArray()
    {
        $request = (new Request('GET', '/any/path'))
            ->withQueryParams(['foo' => 'bar'])
            ->withBody(Utils::streamFor(json_encode(['answer' => 42])));

        self::assertSame([
            'foo' => 'bar',
            'answer' => 42,
        ], $request->get());
    }

    /** @test */
    public function getProtocolReturnsTheProtocolTheClientUsed()
    {
        // by default (on cli) it is http
        $request = new Request('GET', '/any/path');

        $protocol = $request->getProtocol();

        self::assertSame('http', $protocol);
    }

    /** @test */
    public function getProtocolReadsServerParamHttps()
    {
        $request = new Request('GET', '/any/path', [], null, '1.1', ['HTTPS' => 'on']);

        $protocol = $request->getProtocol();

        self::assertSame('https', $protocol);
    }

    /** @test */
    public function getProtocolAcceptsXForwardedProtoForTrustedProxies()
    {
        /** @var m\MockInterface|Request $request */
        $request = m::mock(Request::class)->makePartial();
        $request->__construct('GET', '/any/path', ['X-Forwarded-Proto' => 'foobar']);
        $request->shouldReceive('isTrustedForward')->once()->andReturn(true);

        $protocol = $request->getProtocol();

        self::assertSame('foobar', $protocol);
    }

    /** @test */
    public function getProtocolReturnsCurrentProtocolIfNoForwardedProtocolGiven()
    {
        /** @var m\MockInterface|Request $request */
        $request = m::mock(Request::class)->makePartial();
        $request->__construct('GET', '/any/path', []);
        $request->shouldReceive('isTrustedForward')->once()->andReturn(true);

        $protocol = $request->getProtocol();

        self::assertSame('http', $protocol);
    }

    /** @test */
    public function getProtocolOffMeansNoSsl()
    {
        $request = new Request('GET', '/any/path', [], null, '1.1', ['HTTPS' => 'off']);

        $protocol = $request->getProtocol();

        self::assertSame('http', $protocol);
    }

    /** @test */
    public function isTrustedForwardIsFalseWhenNoProxiesAreTrusted()
    {
        $this->app->config->trustedProxies = [];
        $request = new Request('GET', '/any/path');

        $trusted = $request->isTrustedForward();

        self::assertFalse($trusted);
    }

    /** @test */
    public function isTrustedForwardIsTrueWhenTheRemoteAddrMatches()
    {
        $this->app->config->trustedProxies = ['127.0.0.1'];
        $request = new Request('GET', '/any/path', [], null, '1.1', ['REMOTE_ADDR' => '127.0.0.1']);

        $trusted = $request->isTrustedForward();

        self::assertTrue($trusted);
    }

    /** @test */
    public function isTrustedForwardIsTrueWhenTheRemoteAddrMatchesAnyRange()
    {
        $this->app->config->trustedProxies = ['127.0.0.1', '10.23.42.0/24'];
        $request = new Request('GET', '/any/path', [], null, '1.1', ['REMOTE_ADDR' => '10.23.42.1']);

        $trusted = $request->isTrustedForward();

        self::assertTrue($trusted);
    }

    /** @test */
    public function isTrustedForwardIsFalseWhenTheProxyIsUntrusted()
    {
        $this->app->config->trustedProxies = ['127.0.0.1', '10.23.42.0/24'];
        $request = new Request('GET', '/any/path', [], null, '1.1', ['REMOTE_ADDR' => '192.168.0.42']);

        $trusted = $request->isTrustedForward();

        self::assertFalse($trusted);
    }

    /** @test */
    public function isSslSecuredIsTrueWhenTheProtocolIsHttps()
    {
        /** @var m\MockInterface|Request $request */
        $request = m::mock(Request::class)->makePartial();
        $request->__construct('GET', '/any/path');
        $request->shouldReceive('getProtocol')->once()->andReturn('https');

        $secured = $request->isSslSecured();

        self::assertTrue($secured);
    }

    /** @test
     * @dataProvider provideAcceptHeaders */
    public function acceptsReturnsIfTheMimeTypeIsAccepted($header, $type, $accepted)
    {
        $request = new Request('GET', '/any/pah', [
            'accept' => $header,
        ]);

        self::assertSame($accepted, $request->accepts($type));
    }

    public function provideAcceptHeaders()
    {
        return [
            ['text/html', 'text/html', true],
            ['text/html', 'application/json', false],
            ['text/html,application/json;q=0.8', 'application/json', true],
            ['image/png,image/*;q=0.8', 'image/jpeg', true],
            ['text/html, application/xhtml+xml', 'application/xhtml+xml', true],
        ];
    }

    /** @test */
    public function acceptsIgnoresAsteriskSlashAsterisk()
    {
        $request = new Request('GET', '/any/pah', [
            'accept' => 'text/html,*/*;q=0.4',
        ]);

        self::assertFalse($request->accepts('application/json'));
    }
}
