<?php

namespace Test\Unit\Service;

use App\Service\IpHelper;
use Test\TestCase;

class IpHelperTest extends TestCase
{
    /** @test
     * @dataProvider ipv4Ranges */
    public function isInRangeWorksWithIpv4Ranges(string $ip, string $range, bool $result)
    {
        self::assertSame($result, IpHelper::isInRange($ip, $range));
    }

    public function ipv4Ranges(): array
    {
        return [
            ['192.168.0.1', '192.168.0.0/24', true],
            ['192.172.168.12', '192.172.168.0/28', true],
            ['192.172.168.12', '192.172.168.0/29', false],
            ['10.192.39.29', '10.0.0.0/8', true],
            ['127.0.1.1', '127.0.0.1/32', false],
        ];
    }

    /** @test
     * @dataProvider ipv6Ranges */
    public function isInRangeWorksWithIpv6Ranges(string $ip, string $range, bool $result)
    {
        self::assertSame($result, IpHelper::isInRange($ip, $range));
    }

    public function ipv6Ranges(): array
    {
        return [
            ['fe80::1', 'fe80::/64', true],
            ['::1', '::1/128', true],
            ['fe80::1', '::1/128', false],
        ];
    }

    /** @test */
    public function isInRangeWorksWithSingleIpAddressesInIpv4()
    {
        self::assertTrue(IpHelper::isInRange('192.168.0.1', '192.168.0.1'));
    }

    /** @test */
    public function isInRangeWorksWithSingleIpAddressesInIpv6()
    {
        self::assertTrue(IpHelper::isInRange('fe80::1', 'fe80:0:0:0:0:0:0:1'));
    }

    /** @test */
    public function isInRangeDoesNotMatchIpsWithDifferentProtocolVersion()
    {
        self::assertFalse(IpHelper::isInRange('192.168.0.1', 'c0a8:1::'));
        self::assertFalse(IpHelper::isInRange('192.168.0.1', 'c0a8:1::/32'));
    }
}
