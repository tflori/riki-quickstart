<?php

namespace Test\Cli\Config;

use App\Config;
use Test\Cli\TestCase;

class CacheTest extends TestCase
{
    /** @test */
    public function stopsWhenCachingIsDisabled()
    {
        $this->mocks['environment']->shouldReceive('canCacheConfig')->with()
            ->once()->andReturn(false);

        $result = $this->start('config:cache');

        self::assertEquals('The environment does not allow to cache the configuration!', trim($result['output']));
        self::assertSame(0, $result['returnVar']);
    }

    /** @test */
    public function failsWhenDirectoryIsAFile()
    {
        touch('/tmp/cache');
        $cachePath = '/tmp/cache/config.spo';
        $this->mocks['environment']->shouldReceive('getConfigCachePath')->with()
            ->once()->andReturn($cachePath);

        $result = $this->start('config:cache');

        self::assertEquals('Cache directory is not writeable!', trim($result['errors']));
        self::assertSame(2, $result['returnVar']);
    }

    /** @test */
    public function cachesTheConfig()
    {
        $cachePath = '/tmp/riki-test-config.spo';
        $this->mocks['environment']->shouldReceive('getConfigCachePath')->with()
            ->once()->andReturn($cachePath);

        $result = $this->start('config:cache');

        self::assertFileExists($cachePath);
        self::assertSame(
            serialize(new Config($this->mocks['environment'])),
            file_get_contents($cachePath)
        );
        self::assertEquals('Configuration cache created successfully!', trim($result['output']));
        self::assertSame(0, $result['returnVar']);
    }

    /** @test */
    public function clearsTheConfigCache()
    {
        $cachePath = '/tmp/riki-test-config.spo';
        $this->mocks['environment']->shouldReceive('getConfigCachePath')->with()
            ->once()->andReturn($cachePath);
        file_put_contents($cachePath, serialize(['pretty' => 'useless']));

        $result = $this->start('config:cache', '--clear');

        self::assertFileNotExists($cachePath);
        self::assertEquals('Configuration cache cleared successfully!', trim($result['output']));
        self::assertSame(0, $result['returnVar']);
    }
}
