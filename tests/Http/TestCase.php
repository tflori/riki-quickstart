<?php

namespace Test\Http;

use App\Http\HttpKernel;
use App\Http\Request;
use Tal\Psr7Extended\ServerResponseInterface;

abstract class TestCase extends \Test\TestCase
{
    protected function get(string $uri, array $query = []): ServerResponseInterface
    {
        $request = (new Request('get', $uri, []))->withQueryParams($query);
        $kernel = new HttpKernel($this->app);
        return $this->app->run($kernel, $request);
    }
}
